# Команда запуска gitlab-runner в режиме docker-in-docker

запускаем раннер

```
docker run -d --name gitlab-runner --restart always \
  -v /var/run/docker.sock:/var/run/docker.sock \
  -v ~/.gitlab-runner:/etc/gitlab-runner \
  gitlab/gitlab-runner:latest
```

регистрация раннера

```
docker exec -ti gitlab-runner gitlab-runner register -n \
  --url https://gitlab.com/ci \
  --registration-token TOKEN \
  --executor docker \
  --description "name" \
  --docker-image "atnartur/docker:latest" \
  --docker-volumes /var/run/docker.sock:/var/run/docker.sock
```

запуск сервиса

`docker exec -ti gitlab-runner gitlab-runner start`