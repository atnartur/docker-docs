# Сборка контейнера data

Создаем файл `Dockerfile` в папке `docker`. Это файл с инструкциями для сборки контейнера data для проекта. Для легковесности контейнеры data обычно собирают на основе образа [alpine](https://hub.docker.com/r/_/alpine/).

Его примерное содержание находится в `files/Dockerfile`.

Там происходит:

1. Копирование папки с конфигурациями nginx в папку контейнера
2. Копирование файлов проекта в контейнер
3. Копирование конфигурации, зависящей от сервера, на ее место
4. Установка зависимостей npm, bower, сборка через gulp
5. Удаление папки docker из контейнера, удаление всех docker-конфигов - этого всего не должно быть в контейнере
6. Установка прав на утилитку `wait-for-it` (помогает контейнеру php ждать, пока произойдет запуск БД)

Этот Dockerfile так же лежит в `files/Dockerfile`. Контейнер data наследуется от образа alpine - очень маленький линукс дистрибутив весом всего лишь в 5МБ.

**Команда сборки образа** (запускается в корневой папке проекта)

```
docker build -t registry.gitlab.com/USERNAME/REPONAME -f docker/Dockerfile .
```

Ключ `-t` отвечает за название тега. Тег `registry.gitlab.com/USERNAME/REPONAME` создает образ, который можно будет отправить в Gitlab registry (хранилище Docker-образов в Gitlab) в репозиторий `USERNAME/REPONAME`. 
 
Ключ `-f` помогает найти Dockerfile для сборки образа.

Подробнее про сборку контейнера можно прочитать в официальной документации: [о Dockerfile](https://docs.docker.com/engine/reference/builder/), [о `docker build`](https://docs.docker.com/engine/reference/commandline/build/)

После того, как образ будет готов, его можно отправить в Gitlab registry следующей командой:

```
docker push registry.gitlab.com/USERNAME/REPONAME
```

Скачивание образа:

```
docker pull registry.gitlab.com/USERNAME/REPONAME
```

На этом сборка контейнера с кодом проекта завершена.