# Docker docs

Документация по Docker-инфраструктуре от [Артура](http://i.atnartur.ru).

![](https://gitlab.com/atnartur/docker-docs/raw/6bc2396e800ee42182d2dcd9e99bc881eb2f56aa/images/docker.png)

[Docker](https://ru.wikipedia.org/wiki/Docker) — программное обеспечение для автоматизации развёртывания и управления приложениями в среде виртуализации на уровне операционной системы, например LXC.

[LXC (англ. Linux Containers)](https://ru.wikipedia.org/wiki/LXC) — система виртуализации на уровне операционной системы для запуска нескольких изолированных экземпляров операционной системы Linux на одном узле. LXC не использует виртуальные машины, а создает виртуальное окружение с собственным пространством процессов и сетевым стеком. Все экземпляры LXC используют один экземпляр ядра операционной системы.

- [Официальный сайт Docker](http://docker.com)
- [Docker Hub - репозиторий образов Docker](http://hub.docker.com)
- [Избушка на docker-ножках или объяснение Docker на примере домов](http://atnartur.ru/posts/2016/docker-house/)
- [Митап сообщества CruelIT с подробным объяснением основ работы docker](https://github.com/CruelIT/meetups/tree/master/2017-02-22%20Docker)